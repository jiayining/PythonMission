"""
   练习18
"""
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from pylab import mpl

mpl.rcParams['font.sans-serif'] = ['SimHei'] #指定使用的字体为黑体
mpl.rcParams['axes.unicode_minus'] = False #正常显示符号

data = pd.read_csv('IMDB-Movie-Data.csv',encoding='gbk')
#print(data)
avg = round(data['Rating'].mean(),2)
print('评分均值',avg)

fig,axes=plt.subplots(nrows=2,ncols=1,figsize=(20,12),dpi=100)
x_list1 = data['Rating']
x_list2 = data['Runtime (Minutes)']
distance1 = 0.2
distance2 = 5
group_num1=int((max(x_list1)-min(x_list1))//distance1) #分成组数
group_num2=int((max(x_list2)-min(x_list2))//distance2)

axes[0].set_title('Rating分布情况图')
axes[0].set_xlabel('得分/分')
axes[0].set_ylabel('数量/个')
axes[0].hist(x_list1,bins=group_num1,width=[0.1],color='blue')
axes[1].set_title('Runtime分布情况图')
axes[1].set_xlabel('时长/分钟')
axes[1].set_ylabel('数量/个')
axes[1].hist(x_list2,bins=group_num2,width=[2],color='orange')
plt.show()

temp_list = data['Genre'].str.split(",").tolist()
genre_list = list(set([i for j in temp_list for i in j]))
zeros_data = pd.DataFrame(np.zeros((data.shape[0], len(genre_list))), columns=genre_list)
for i in range (data.shape[0]):
    zeros_data.loc[i,temp_list[i]] = 1

genre_count = zeros_data.sum(axis=0)
print(genre_count)