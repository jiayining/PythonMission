"""
   练习9
"""
import re
import sys
distone = {}
numTen = []
#将文档变为字典
with open(r'C:\Users\Lenovo\Desktop\The Old Man and the Sea.txt','r',encoding = 'utf-8',errors = 'ignore') as f:
    f_Data = f.readlines()
#去掉非字符的符号
for line in f_Data:
    line = re.sub('\W'," ",line)
    lineone = line.split()
    for keyone in lineone:
        if not distone.get(keyone):
            distone[keyone] = 1
        else:
            distone[keyone] += 1
f.close()
#整理前10出现的单词的次数
for v in distone.values():
    if v not in numTen:
        numTen.append(v)
numTen.sort()
numTen = numTen[-10:]
#将字典转为列表
distone = sorted(distone.items(),key=lambda d:d[1],reverse = True)
#遍历
for i in distone:
    if i[1] in numTen:
        print(i)
