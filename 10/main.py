"""
   练习10
"""
def triangle():
    while True:
        a = input("请输入直角三角形第一条直角边的长度:")
        try:
            a = float(a)
        except ValueError as e:
            print("a不是数字！")
        else:
            if a <= 0:
                print("a需为正数！")
            else:
                break
    while True:
        b = float( input("请输入直角三角形第二条直角边的长度:") )
        try:
            b = float(b)
        except ValueError as e:
            print("b不是数字！")
        else:
            if b <= 0:
                print("b需为正数！")
            else:
                break
    return (a * b) / 2
print(triangle())