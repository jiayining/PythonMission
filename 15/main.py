"""
   练习15
"""
class Pokemon(object):
    attribute = "pokemon"

    def __init__(self, name):
        self.name = name


class Water_Pokemon(Pokemon):
    attribute = "水系" + Pokemon.attribute

    def WaterSkill(self):
        print("技能：水枪")


class Fire_Pokemon(Pokemon):
    attribute = "火系" + Pokemon.attribute

    def FireSkill(self):
        print("技能：喷火")


class Grass_Pokemon(Pokemon):
    attribute = "草系" + Pokemon.attribute

    def GrassSkill(self):
        print("技能：飞叶快刀")


a = Water_Pokemon("杰尼龟")
print(a.name, a.attribute)
a.WaterSkill()

b = Fire_Pokemon("小火龙")
print(b.name, b.attribute)
b.FireSkill()

c = Grass_Pokemon("妙蛙种子")
print(c.name, c.attribute)
c.GrassSkill()

class New_Pokemon(Water_Pokemon, Grass_Pokemon, Fire_Pokemon):
       pass
d = New_Pokemon("newPokemon")
print(d.name)
d.WaterSkill()