from django.db import models


# Create your models here.
class User(models.Model):
    mid = models.AutoField(primary_key=True)  # 用户mid
    name = models.TextField()  # 用户名
    signature = models.TextField()  # 用户个性签名
    level = models.IntegerField()  # 用户等级
    fans = models.IntegerField()  # 用户粉丝数
    vip = models.BooleanField()  # 用户是否是大会员
    date = models.DateTimeField(auto_now=True)  # 获取用户数据的时间

def __str__(self):
    return self.name
