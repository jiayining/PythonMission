"""
   练习11
"""
import random

def get():
    redBall = set()
    while len(redBall) < 6:
        redBall.add(random.randint(1,33))
    double_color_ball = list(redBall)
    double_color_ball.sort()
    double_color_ball.append(random.randint(1,16))
    return double_color_ball

n = input("需要多少注:")
l = list()
try:
    n = int(n)
except TypeError as e:
    print("请输入正整数!")
else:
    if(n <= 0):
        print("请输入正整数!")
    else:
        for i in range(n):
            l.append(get())

print("是否将结果保存到文件 Y/N")
if input().upper() == 'Y':
    with open("double_color_ball.txt","w") as f:
        for i in range(n):
            str2 = "红球："
            for j in range(6):
                str2 = str2 + str(l[i][j]) + " "
            f.write(str2 + "蓝球：" + str(l[i][6]) + "\n")
    print("写入完成")
else:
    for i in range(n):
        print("第",i,"注——红球：",end = "")
        for j in range(6):
             print(l[i][j],end = " ")
        print("蓝球：",l[i][6])
