"""
   练习13
"""
import shutil
import os
import random

suffx_list = ['.jpg','.mp4','.txt',
              '.docx','.pptx','.xlsx',
              '.png','.py','.c','.cpp',
              '.exe','.mp3','.gif',
              '.o','.px','.q',
              ]
path1 = r'C:\Users\Lenovo\PycharmProjects\Mypython\13\杂乱的文件目录'
os.makedirs(path1,exist_ok=True)
for i in range(10000):
    filepath=os.path.join(path1,str(random.randint(0,99999999999))+suffx_list[
        random.randrange(0,len(suffx_list))])
    with open(filepath,'w') as f:
           pass

path2 = r'C:\Users\Lenovo\PycharmProjects\Mypython\13\分类的文件目录'
os.makedirs(path2, exist_ok=True)
for curdir, dirs, files in os.walk(path1):
    for file in files:
        last_name = file.split('.')[1]  # 获取文件后缀名
        path3 = ''
        path3 = path2 + '\\' + last_name
        os.makedirs(path3, exist_ok=True)
        shutil.move(path1 + '\\' + file, path3 + '\\' + file)

path4 = r'C:\Users\Lenovo\PycharmProjects\Mypython\13\归档的文件目录'
for curdir, dirs, files in os.walk(path2):
    for dir in dirs:
        shutil.make_archive(path4 + '\\' + dir, 'zip', path2 + '\\' + dir)