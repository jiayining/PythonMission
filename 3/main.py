"""
   练习3
"""
print("请输入一个整数:")
a = input()
a = int(a)
if(a < 1 or a > 10):
   print("ERROR")
else:
   for i in range(1 , a + 1):
      space = a - i
      for j in range(1 , space + 1):
         print(" ", end="")
      star = 2 * i - 1
      for j in range(1 , star + 1):
         print("*", end="")
      print("")