from django.shortcuts import render
from django.http import HttpResponse
from .models import User
from django.core.paginator import Paginator


# Create your views here.
def hello_django(request):
    return HttpResponse("Hello Django!")


def index(request):
    page = request.GET.get('page')
    if page:
        page = int(page)
    else:
        page = 1
    all_users = User.objects.all()
    paginator = Paginator(all_users, 6)
    page_num = paginator.num_pages
    page_user_list = paginator.page(page)
    if page_user_list.has_next():
        next_page = page + 1
    else:
        next_page = page
    if page_user_list.has_previous():
        previous_page = page - 1
    else:previous_page = page
    if len(range(1, page_num)) < 5:
        page_num = range(1, page_num)
    else:
        if page > 4:
            page_num = range(page - 1, page + 5)
        else:
            page_num = range(1, 7)
    top10_list = User.objects.order_by('-fans')[:10]
    print(type(all_users))
    context = {
        'user_list': page_user_list,
        'page_num': page_num,
        'curr_page': page,
        'next_page': next_page,
        'previous_page': previous_page,
        'top10_list': top10_list,
    }
    return render(request, 'bilibili/index.html', context=context)


def detail(request, mid):
    user_queryset = User.objects.filter(mid=mid)
    if len(user_queryset) != 1:
        return HttpResponse("文章不存在")
    else:
        next_user = User.objects.filter(mid__gt=mid).order_by("mid")[:1]
        if len(next_user) != 0:
            next = {
                'next_mid': next_user[0].mid,
                'next_name': next_user[0].name,
            }
        else:
            next = {
                'next_mid': '#',
                'next_name': "没有更靠后的用户了",
            }
        pre_user = User.objects.filter(mid__lt=mid).order_by("-mid")[:1]
        if len(pre_user) != 0:
            pre = {
                'pre_mid': pre_user[0].mid,
                'pre_name': pre_user[0].name,
            }
        else:
            pre = {
                'pre_mid': '#',
                'pre_name': "没有更靠前的用户了",
            }
    context = {
        'user': user_queryset[0],
        'next': next,
        'pre': pre,
    }
    return render(request, 'bilibili/detail.html', context=context)
