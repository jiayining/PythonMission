from django.urls import path
from .views import hello_django  # 导入我们的刚才写好的处理函数
from .views import index
from .views import detail

urlpatterns = [
    path('hello', hello_django),
    path('', index),
    path('detail/<int:mid>', detail)
]
