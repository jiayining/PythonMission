import time
import requests

headers = {
    'User-Agent': "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.164 Safari/537.36 Edg/91.0.864.71",
}
with open("D:/code/web/bilibili_user.csv", 'w',encoding='gb18030', errors='ignore')as f:
    f.write(','.join(['用户编号', '用户名', '用户等级', '用户个性签名','用户粉丝数量','用户是否是大会员','用户是否存在\n']))
    for i in range(1, 1201):
        time.sleep(1)
        print(i)
        url1 = "https://api.bilibili.com/x/space/acc/info?mid={}&jsonp=jsonp".format(i)
        url2 = "https://api.bilibili.com/x/relation/stat?vmid={}&jsonp=jsonp".format(i)
        r1 = requests.get(url=url1, headers=headers)
        r2 = requests.get(url=url2, headers=headers)
        result = r1.json()
        follower = r2.json()
        if result['code'] == 0:
            f.write(','.join([
                str(result['data']['mid']),
                str(result['data']['name']),
                str(result['data']['level']),
                str(result['data']['sign']),
                str(follower['data']['follower']),
                str(result['data']['vip']['status']),
                str('用户存在') + '\n',
            ]))
        else:
            f.write(','.join([
                str(''),
                str(''),
                str(''),
                str(''),
                str(''),
                str(''),
                str('用户已注销') + '\n',
            ]))
print("爬取完成")