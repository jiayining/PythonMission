"""
   练习16
"""
import csv

import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from pylab import mpl#导入mpl用于修改配置

mpl.rcParams['font.sans-serif'] = ['SimHei'] #指定使用的字体为黑体
mpl.rcParams['axes.unicode_minus'] = False   #正常显示符号

data = pd.read_csv('bilibili_user.csv',encoding='gbk')
data = np.array(data)
print(data)

data_1 = data[:,2]
data_1 =np.array(data_1)
#print(data_1)

lv1 =0
lv2 =0
lv3 =0
lv4 =0
lv5 =0
lv6 =0
other =0
for i in data[:,2]:
    if i==1.0:
        lv1 = lv1 +1
    elif i==2.0:
        lv2 = lv2 +1
    elif i ==3.0:
        lv3 = lv3 +1
    elif i == 4.0:
        lv4 = lv4 + 1
    elif i==5.0:
        lv5 = lv5 +1
    elif i==6.0:
        lv6 = lv6 +1
    else:
        other = other+1


explode=[0.01,0.01,0.01,0.01,0.01,0.01]#设定各项距离圆心n个半径
plt.figure(figsize=(10,10),dpi=100)
x_list=[lv1,lv2,lv3,lv4,lv5,lv6]
y_list=['lv1','lv2','lv3','lv4','lv5','lv6']
plt.pie(x_list,labels=y_list,explode=explode ,autopct="%.1ff%%")
plt.title("level")
plt.show()


plt.bar(y_list,x_list,width=0.3)
plt.title("level")
plt.show()

with open('bilibili_user.csv', 'r') as f:
    reader = csv.reader(f)
    header_row = next(reader)

    t_level = []
    y_nums = []
    x_id = []
    for row in reader:
        if row[1] != ' ': #只考虑存在用户的等级
            x_id.append(row[0])
            t_level.append(row[2] + '级')

    y_nums = sorted(list(set(t_level))) #删除重复数据
    x_level = [t_level.count(i) for i in y_nums]


plt.figure(figsize=(10, 20), dpi=100)
plt.title('用户id与等级散点图')
plt.xlabel('等级/级')
plt.ylabel('用户id')
y_ticks = range(501)
plt.yticks(y_ticks[::50])
plt.scatter(t_level, x_id)
plt.show()