import csv
import os
import django
import pandas as pd

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'web.settings')
django.setup()

from bilibili.models import User

with open('bilibili_user.csv', 'r', encoding='gb18030') as f:
    read = csv.reader(f)
    for row in read:
        if len(row) != 0 and row[0].isdigit() == True and len(row) == 7:
            user = User()
            user.mid = int(row[0])
            user.name = row[1]
            user.level = int(row[2])
            if row[3] == '':
                user.signature = '暂未设置个性签名'
            else:
                user.signature = row[3]
            user.fans = int(row[4])
            if row[5] == '1':
                user.vip = True
            else:
                user.vip = False
            user.save()
            print("saved{}".format(row[0]))
